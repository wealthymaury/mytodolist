//
//  TodoList.swift
//  MyTodoList
//
//  Created by Mauricio Manjarrez Magallón on 03/10/16.
//  Copyright © 2016 Mauricio Manjarrez Magallón. All rights reserved.
//

import UIKit

class TodoList: NSObject {
    var items: [String] = []
    
    func addItem(item: String) {
        items.append(item)
    }
}

extension TodoList: UITableViewDataSource {
    // dice cuantas celdas se van a pintar
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    // se manda llamar por cada item en el array para regresar una celda configurada
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let item = items[indexPath.row]
        
        cell.textLabel!.text = item
        
        return cell
    }
}
