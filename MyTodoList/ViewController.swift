//
//  ViewController.swift
//  MyTodoList
//
//  Created by Mauricio Manjarrez Magallón on 01/10/16.
//  Copyright © 2016 Mauricio Manjarrez Magallón. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let todoList = TodoList()
    
    // @IBOutlet supongo es para hacerlo disponible al storyboard
    // El signo de ! es para obligar a obtener el valor del obcional
    @IBOutlet weak var itemTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    // @IBOutlet vuelve disponible la funcion desde la interface
    @IBAction func addButtonPressed(sender: UIButton){
        print("Agregando elemento: \(itemTextField.text!)")
        todoList.addItem(item: itemTextField.text!)
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = todoList
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

